import java.util.LinkedList;
import java.util.List;

public class DNFGenerator {
	
	
	private static final String v = "v";
	private static final String not = "¬";
	private static final String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	//genere dans l'ordre
	//probanot probabilité qu'une variable soit negative
	//probava probabilité d'apparition d'une variable
	//
	public static String generateDNFSorted(int nbVar, int nbTermes, double probaNot, double probaVar) {
		String res = "";
		for (int i = 0 ; i < nbTermes; i++) {
			for (int j = 0 ; j < nbVar ; j++){
				if (Math.random() < probaVar) 
					if (Math.random() < probaNot) 
						res += not;
					res += letters.charAt(j);	
			}
			res += v;
		}
		res = res.substring(0,res.length() - 1);
		System.out.println("Génération de "+res);
		return res;
	}

	//genere les termes avec un ordre aleatoire pour les variables
	public static String generateDNF(int nbVar, int nbTermes, double probaNot, double probaVar) {
		String res = "";
		for (int i = 0 ; i < nbTermes; i++) {
			for (int j = 0 ; j < nbVar ; j++){
				if (Math.random() < probaVar) 
					if (Math.random() < probaNot) 
						res += not;
					res += letters.charAt((int)(Math.random() * nbVar) );	
			}
			res += v;
		}
		res = res.substring(0,res.length() - 1);
		//System.out.println("Génération de "+res);
		return res;
	}
	
	public static Formule generateDNF2(int nbVar, int nbTermes, int tailleMinTerme, 
			double probaNot, double probaVar, boolean aleatoire) {
		Formule formule = new Formule(nbVar);
		VariableFactory vf = new VariableFactory(nbVar, aleatoire);
		//List<Terme> formule = new LinkedList<Terme>();
		for (int i = 0 ; i < nbTermes; i++) {
			Terme terme = new Terme();
				int j = 0;
				while(j < nbVar){
					Variable var = vf.getVariable();
					formule.ajouterVariable(var);
					if (Math.random() < probaVar)
						terme.ajouterVariable(var, Math.random() < 1 - probaNot);
					j++;
				}
				if(terme.taille() > tailleMinTerme)
					formule.ajouterTerme(terme);

				vf.reset();
			}
		return formule;
		
	}
	
}

