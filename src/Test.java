public class Test {

	public static void main(String [] args) {
		//si aleatoire = true alors les variables apparaitront dans un ordre aleatoire sinon dans l'ordre
		boolean aleatoire = true;
		//nb de variables differentes dans la formule
		int nbVar = 5;
		//nb de termes maximum dans la formule
		int nbMaxTermes = 5;
		//taille minimum du terme ( en variables )
		int tailleMinTerme = 1;
		//proba d'aparition qu"un variable soit negative
		double probaNot = 0.3;

		//proba d'aparition qu"une variable apparraisse dans chaque terme
		double probaVar = 0.7;
		Formule formule = DNFGenerator.generateDNF2(nbVar, nbMaxTermes, tailleMinTerme, probaNot, probaVar, aleatoire);
		Listes.afficherListeVar(formule.variables());
		System.out.println("Formule : "+formule.afficher());
		
		
		
		//formule.etendre();
		//System.out.println("formule etendue : "+formule.afficher());
		
		formule.enleverDoublons();
		System.out.println("Formule classee : "+formule.afficher());
		
		
		//solutions pas opti du tout !!
		ListeSolutions solutions = new ListeSolutions(nbVar);
		solutions.trouverSolutions(formule).afficher();
		System.out.println("fin.");
		
		
		//Sauvegarde dans le trie
		Trie t = new Trie();
		t.save(solutions.solutions);
		System.out.println("Recherche de l'ensemble des solutions trouvées dans le Trie:");
		for (boolean[] s: solutions.solutions)
		{
			for(int i = 0; i<s.length;i++) {
				System.out.print(((s[i])?1:0)+", ");
			}
			System.out.println(t.search(s));
		}	
	};
}
