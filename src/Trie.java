import java.util.List;

public class Trie {
	
	private static TrieNode root;
	static final int BINARY_SIZE = 2;
	
	Trie()
	{
		root = new TrieNode();
	}
	
	static class TrieNode 
    { 
        TrieNode[] children = new TrieNode[BINARY_SIZE]; 
       
        // isEndOfWord is true if the node represents 
        // end of a word 
        boolean isEndOfWord; 
        
        TrieNode(){ 
            isEndOfWord = false; 
            for (int i = 0; i < BINARY_SIZE; i++) 
                children[i] = null; 
        } 
    }; 
    
    public void insert(boolean[] solution) 
    { 
        int level; 
        int length = solution.length;  
       
        TrieNode pCrawl = root; 
       
        for (level = 0; level < length; level++) 
        { 
        	boolean index = solution[level];
        	
        	int val = (index) ? 1 : 0;
            if (pCrawl.children[val] == null) 
                pCrawl.children[val] = new TrieNode(); 
       
            pCrawl = pCrawl.children[val]; 
        } 
       
        // mark last node as leaf 
        pCrawl.isEndOfWord = true; 
    } 
	
	
	
	boolean search(boolean[] solution) 
    { 
        int level; 
        int length = solution.length;  
        TrieNode pCrawl = root; 
       
        for (level = 0; level < length; level++) 
        { 
        	boolean index = solution[level];
        	int val = (index) ? 1 : 0;
       
            if (pCrawl.children[val] == null) 
                return false; 
       
            pCrawl = pCrawl.children[val]; 
        } 
       
        return (pCrawl != null && pCrawl.isEndOfWord); 
    } 
	
	public void save(List<boolean[]> solutions)
	{
		for(boolean[] sol : solutions) {
			insert(sol);
		}
	}
	

}
