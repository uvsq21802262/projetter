import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Terme {
	

	private static final String not = "¬";
	
	List<Variable> positifs;
	List<Variable> negatifs;
	public Terme() {
		positifs = new ArrayList<Variable>();
		negatifs = new ArrayList<Variable>();
	}
	
	public boolean contient(Variable v) {
		for (int i= 0 ; i < positifs.size() ; i++) {
			if ( positifs.get(i) == v ) 
				return true;
			if ( negatifs.get(i) == v )
				return true;
		}
		return false;
	}
	public void ajouterVariable(Variable v, boolean signe) {

		if (contient(v)) 
			return;
		if (signe) {
			positifs.add(v);
			negatifs.add(null);
		} else {
			positifs.add(null);
			negatifs.add(v);
		}
	}

	public int taille(){
		return positifs.size();
	}
	
	public String toString() {
		String res = "";
		for(int i = 0 ; i < positifs.size() ; i++) {
			if ( positifs.get(i) == null ) {
				res += not+negatifs.get(i).toString();
			} else {
				res += positifs.get(i).toString();
			}
		}
		return res;
	}
	
	public List<Terme> etendre(List<Variable> variables) {
		System.out.println("on etend "+this);
		//System.out.print("vars dispo : "); Listes.afficherListeVar(variables);
		//il faut trier le terme avant et variables
		List<Terme> termes = new ArrayList<Terme>();
		
		if(variables.size() != 0) {
			Variable v = variables.remove(0);
			Terme t1 = this.clone();
			Terme t2 = this.clone();
			//id =  la place de v dans le terme
			t1.positifs.add(v.id, v);
			t1.negatifs.add(v.id, null);
			t2.positifs.add(v.id, null);
			t2.negatifs.add(v.id, v);
			termes.addAll(t1.etendre(variables));
			termes.addAll(t2.etendre(variables));
		} else {
			termes.add(this);
		}
		return termes;
	}
	
	public List<Variable> variables() {
		List<Variable> variables = new ArrayList<>();
		for (int i = 0 ; i < positifs.size() ; i++) {
			Variable v;
			if ( positifs.get(i) == null ) {
				v = negatifs.get(i);
			} else {
				v = positifs.get(i);
			}
			variables.add(v);
		}
		return variables;
	}
	
	
	private void triFusion(List<Variable> variables, int d, int f) {
		if ( f - d == 0 ) 
			return;
		int milieu = ( d + f ) / 2; 
		triFusion(variables, d, milieu);
		triFusion(variables, milieu + 1, f);
		fusion(variables, d, milieu, f);
	}
	private static void afficherSousTableau(List<Variable> variables, int d, int f){
		for(int i = d; i <= f;i++){
			System.out.print(variables.get(i));
		}
		System.out.println();
	}
	private void fusion(List<Variable> variables, int d1, int f1, int f2) {
		int d2 = f1 + 1;
		Variable tmp[] = new Variable[f1-d1+1];
		boolean signe[] = new boolean[f1-d1+1];
		for(int i = d1 ; i <= f1 ; i++) {
			tmp[i - d1] = variables.get(i);
			signe[i - d1] = (positifs.get(i) != null);
		}
		
		int compt1=d1;
        int compt2=d2;
        
        for (int i = d1 ; i <= f2 ; i++) {     
            if (compt1 == d2) { //c'est que tous les éléments du premier tableau ont été utilisés
                break; //tous les éléments ont donc été classés
            }
            else if (compt2 == (f2 + 1))  {//c'est que tous les éléments du second tableau ont été utilisés
                variables.set(i, tmp[compt1-d1]); //on ajoute les éléments restants du premier tableau
                if (signe[compt1-d1]) {
                	positifs.set(i, tmp[compt1-d1]);
                	negatifs.set(i, null);
                } else {
                	negatifs.set(i, tmp[compt1-d1]);
                	positifs.set(i, null);
                }
                compt1++;
               }
            else 
            if (tmp[compt1-d1].id < variables.get(compt2).id) {
            	Variable var = tmp[compt1-d1];
                variables.set(i, var); //on ajoute un élément du premier tableau
                if (signe[compt1-d1]) {
                	positifs.set(i, var);
                	negatifs.set(i, null);
                } else {
                	negatifs.set(i, var);
                	positifs.set(i, null);
                }
                compt1++;
            }
            else {
                variables.set(i, variables.get(compt2)); //on ajoute un élément du second tableau
                positifs.set(i, positifs.get(compt2));
                negatifs.set(i, negatifs.get(compt2));
                compt2++;
            }
        }
	}
	
	public Terme clone() {
		Terme t = new Terme();
		t.positifs.addAll(this.positifs);
		t.negatifs.addAll(this.negatifs);
		return t;
	}
	public void trierTerme() {
		List<Variable> variables = variables();
		//Collections.sort(variables);
		triFusion(variables, 0, variables.size() - 1);
	}
	
	public boolean equals(Terme t) {
		int n = t.taille();
		if (this.taille() != n) return false;
		for (int i = 0 ; i < n ; i++) {
			if (t.positifs.get(i) != this.positifs.get(i)) {
				return false;
			}
		}
		return true;
	}
	
	public boolean estDansListe(List<Terme> termes) {
		for (Terme t : termes) {
			if (t.equals(this))
				return true;
		}
		return false;
	}

	
}
