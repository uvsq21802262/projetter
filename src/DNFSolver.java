
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class DNFSolver {

	private static final String v = "v";
	private static final String not = "¬";

	public DNFSolver() {
	}
	
	private static void printHashMap(HashMap<String, Boolean> hash) {
		System.out.print("\t");
		for (String s : hash.keySet()) {
			System.out.print(s + "," + ((hash.get(s)) ? 1 : 0) + " | ");
		}
		System.out.println("");
	}

	private static void printList(List<String> list) {
		for (String s : list) {
			System.out.print(s+" ");
		}
		System.out.println("");
	}

	// variables liste de toutes les variables présentes dans la formule
	// terme : terme à entendre : si variables = {a,b,c) alors ab devient abc +
	// abnon(c)
	private static List<String> etendreTerme(String terme, List<String> variables) {
		List<String> termes = new LinkedList<String>();
		for (int i = 0 ; i < variables.size() ; i++ ) {
			String var = variables.get(i);
			if (!terme.contains(var)) {
				termes = etendreTerme("" + terme + var, variables);
				termes.addAll(etendreTerme("" + terme + not + var, variables));
				
				return termes;
			}
		}
		
		termes.add(terme);
		return termes;
			
	}

	// string terme terme non trié à ajouter a solution
	// solution hash map des solutions
	// vars solution du terme à ajouter à solution
	private static String triTerme(String terme) {
		//System.out.println("Lecture de : " + terme);
		HashMap<String, Boolean> vars = new HashMap<String, Boolean>();
		// set<key, boolean> entry is true if the key/var is negated, false
		// otherwise
		String token;
		boolean positive = true;
		for (int i = 0; i < terme.length(); i++) {// remplit la hash map
			token = terme.substring(i, i + 1);
			if (token.equals(not)) {// negation variable
				positive = false;
				continue;
			} else {// variable
				if (vars.containsKey(token)) {
					if (vars.get(token) != positive) {// solution impossible
						System.out.println("Le terme " + terme
								+ " n'est pas satisfaisable");
						return "";
					}
				} else {// nouvelle variable
					vars.put(token, positive);
				}
			}
			positive = true;
		}

		List<String> variables = new LinkedList<String>(vars.keySet());
		Collections.sort(variables);// tri dans l'ordre alphabetique
		String res = "";
		for (String var : variables) {
			if (!vars.get(var)) {
				res = res + not;
			}
			res = res + var;
		}
		return res;

	}

	private static String triFormule(String formule) {
		List<String> termes = decompositionFormule(formule);
		List<String> termesTries = new LinkedList<String>();
		for(String terme: termes) {
			String termeTrie = triTerme(terme);
			if (!termesTries.contains(termeTrie))
				termesTries.add(termeTrie);
			else {
				System.out.println("Doublon de "+ terme + " : " + termeTrie);
			}
		}
		return String.join(v, termesTries);
	}
	
	private static List<String> decompositionFormule(String formule) {
		List<String> termes = new LinkedList<String>();
		String temp = "";
		String token;
		for (int i = 0; i < formule.length(); i++) {
			token = formule.substring(i, i + 1);
			if (token.equals(v)) {
				termes.add(temp);
				temp = "";
			} else {
				temp += token;
			}
		}
		termes.add(temp);
		return termes;
	}

	private static String etendreFormule(List<String> variables, String formule) {
		List<String> termes = decompositionFormule(formule);
		List<String> newTermes = new LinkedList<String>();
		for (String terme : termes) {
			String tmp = triTerme(terme);
			if (tmp == "") continue;
			for (String str : etendreTerme(tmp, variables)) {
				if (!newTermes.contains(str)) 
					newTermes.add(str);
			}
		}
		return String.join(v, newTermes);
	}

	private static List<String> variables(String str) {
		List<String> variables = new ArrayList<String>();
		String var = "";
		for (int i = 0; i < str.length(); i++) {
			var = str.substring(i, i + 1);
			if (!var.equals(not) && !var.equals(v) && !variables.contains(var))
				variables.add((var));
		}

		Collections.sort(variables);
		System.out.print("variables : ");
		printList(variables);
		return variables;
	}

	private static void solutionFormule(String formule) {
		List<String> termes = decompositionFormule(formule);
		for (String terme : termes) {
			HashMap<String, Boolean> solution = solutionTerme(terme);
			printHashMap(solution);
		}
	}
	
	private static HashMap<String, Boolean> solutionTerme(String terme) {
		HashMap<String, Boolean> solution = new HashMap<String, Boolean>();
		// set<key, boolean> entry is true if the key/var is negated, false otherwise
		String token;
		boolean positive = true;
		for (int i = 0; i < terme.length(); i++) {// remplit la hash map
			token = terme.substring(i, i + 1);
			if (token.equals(not)) {// negation variable
				positive = false;
				continue;
			} else {// variable
				if (solution.containsKey(token)) {
					if (solution.get(token) != positive) {// solution impossible
						System.out.println("Le terme " + terme
								+ " n'est pas satisfaisable");
						return null;
					}
				} else {// nouvelle variable
					solution.put(token, positive);
				}
			}
			positive = true;
		}
		return solution;
	}

	public static void solve(String str) {
		System.out.println("Formule : "+str);
		str = etendreFormule(variables(str), str);
		System.out.println("formule etendue : "+str);
		str = triFormule(str);
		System.out.println("formule triée : " + str);
		
		System.out.println("solution :");
		solutionFormule(str);

	}

	
//	private static void printHashMap(HashMap<String, Boolean> hash) {
//		System.out.print("\t");
//		for (String s : hash.keySet()) {
//			System.out.print(s + "," + ((hash.get(s)) ? 1 : 0) + " | ");
//		}
//		System.out.println("");
//	}
//
//	private static void printList(List<String> list) {
//		for (String s : list) {
//			System.out.print(s+" ");
//		}
//		System.out.println("");
//	}

	// variables liste de toutes les variables présentes dans la formule
	// terme : terme à entendre : si variables = {a,b,c) alors ab devient abc +
	// abnon(c)
//	private static List<String> etendreTerme(String terme, List<String> variables) {
//		List<String> termes = new LinkedList<String>();
//		for (int i = 0 ; i < variables.size() ; i++ ) {
//			String var = variables.get(i);
//			if (!terme.contains(var)) {
//				termes = etendreTerme("" + terme + var, variables);
//				termes.addAll(etendreTerme("" + terme + not + var, variables));
//				
//				return termes;
//			}
//		}
//		
//		termes.add(terme);
//		return termes;
//			
//	}

	// string terme terme non trié à ajouter a solution
	// solution hash map des solutions
	// vars solution du terme à ajouter à solution
//	private static String triTerme(String terme) {
//		//System.out.println("Lecture de : " + terme);
//		HashMap<String, Boolean> vars = new HashMap<String, Boolean>();
//		// set<key, boolean> entry is true if the key/var is negated, false
//		// otherwise
//		String token;
//		boolean positive = true;
//		for (int i = 0; i < terme.length(); i++) {// remplit la hash map
//			token = terme.substring(i, i + 1);
//			if (token.equals(not)) {// negation variable
//				positive = false;
//				continue;
//			} else {// variable
//				if (vars.containsKey(token)) {
//					if (vars.get(token) != positive) {// solution impossible
//						System.out.println("Le terme " + terme
//								+ " n'est pas satisfaisable");
//						return "";
//					}
//				} else {// nouvelle variable
//					vars.put(token, positive);
//				}
//			}
//			positive = true;
//		}
//
//		List<String> variables = new LinkedList<String>(vars.keySet());
//		Collections.sort(variables);// tri dans l'ordre alphabetique
//		String res = "";
//		for (String var : variables) {
//			if (!vars.get(var)) {
//				res = res + not;
//			}
//			res = res + var;
//		}
//		return res;
//
//	}
//
//	private static String triFormule(String formule) {
//		List<String> termes = decompositionFormule(formule);
//		List<String> termesTries = new LinkedList<String>();
//		for(String terme: termes) {
//			String termeTrie = triTerme(terme);
//			if (!termesTries.contains(termeTrie))
//				termesTries.add(termeTrie);
//			else {
//				System.out.println("Doublon de "+ terme + " : " + termeTrie);
//			}
//		}
//		return String.join(v, termesTries);
//	}
//	
//	private static List<String> decompositionFormule(String formule) {
//		List<String> termes = new LinkedList<String>();
//		String temp = "";
//		String token;
//		for (int i = 0; i < formule.length(); i++) {
//			token = formule.substring(i, i + 1);
//			if (token.equals(v)) {
//				termes.add(temp);
//				temp = "";
//			} else {
//				temp += token;
//			}
//		}
//		termes.add(temp);
//		return termes;
//	}

//	private static String etendreFormule(List<String> variables, String formule) {
//		List<String> termes = decompositionFormule(formule);
//		List<String> newTermes = new LinkedList<String>();
//		for (String terme : termes) {
//			String tmp = triTerme(terme);
//			if (tmp == "") continue;
//			for (String str : etendreTerme(tmp, variables)) {
//				if (!newTermes.contains(str)) 
//					newTermes.add(str);
//			}
//		}
//		return String.join(v, newTermes);
//	}
//
//	private static List<String> variables(String str) {
//		List<String> variables = new ArrayList<String>();
//		String var = "";
//		for (int i = 0; i < str.length(); i++) {
//			var = str.substring(i, i + 1);
//			if (!var.equals(not) && !var.equals(v) && !variables.contains(var))
//				variables.add((var));
//		}
//
//		Collections.sort(variables);
//		System.out.print("variables : ");
//		printList(variables);
//		return variables;
//	}
//
//	private static void solutionFormule(String formule) {
//		List<String> termes = decompositionFormule(formule);
//		for (String terme : termes) {
//			HashMap<String, Boolean> solution = solutionTerme(terme);
//			printHashMap(solution);
//		}
//	}
//	
//	private static HashMap<String, Boolean> solutionTerme(String terme) {
//		HashMap<String, Boolean> solution = new HashMap<String, Boolean>();
//		// set<key, boolean> entry is true if the key/var is negated, false otherwise
//		String token;
//		boolean positive = true;
//		for (int i = 0; i < terme.length(); i++) {// remplit la hash map
//			token = terme.substring(i, i + 1);
//			if (token.equals(not)) {// negation variable
//				positive = false;
//				continue;
//			} else {// variable
//				if (solution.containsKey(token)) {
//					if (solution.get(token) != positive) {// solution impossible
//						System.out.println("Le terme " + terme
//								+ " n'est pas satisfaisable");
//						return null;
//					}
//				} else {// nouvelle variable
//					solution.put(token, positive);
//				}
//			}
//			positive = true;
//		}
//		return solution;
//	}
	

//	public static void solve(List<Terme> formule) {
//		System.out.print("Formule : " + Terme.afficherFormule(formule));
//		//str = etendreFormule(variables(str), str);
//		//System.out.println("formule etendue : "+str);
//		//str = triFormule(str);
//		System.out.println("formule triée : " + str);
//		
//		System.out.println("solution :");
//		solutionFormule(str);
//
//	}
}