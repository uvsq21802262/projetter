
public class VariableFactory {

	private int taille;
	private boolean aleatoire;
	public int dernier;
	private Variable[] variables;
	public VariableFactory(int n, boolean aleatoire) {
		taille = n;
		this.aleatoire = aleatoire;
		init();
		reset();
	}

	
	public void init() {
		variables = new Variable[taille];
		for (int i = 0; i < taille ; i++) 
			variables[i] = new Variable();
	}
	
	public void  reset() {
		if(aleatoire) 
			dernier = taille;
		else dernier = 0;
	}
	
	public Variable getVariable() {
		if (aleatoire) {
			int i = (int)(dernier * Math.random());
			dernier--;
			Variable tmp = variables[i];
			variables[i] = variables[dernier];
			variables[dernier] = tmp;
			return tmp;
		} else {
			return variables[dernier++];
		}
	}
}
