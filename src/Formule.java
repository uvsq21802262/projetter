import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


public class Formule {

	private static final String and = " v ";
	public int nbVar;
	public List<Terme> termes;

	public HashMap<Variable, Object> variables;
	public Formule(int n) {
		nbVar = n;
		termes = new ArrayList<Terme>();
		variables = new HashMap<Variable, Object>();
	}
	
	public void ajouterVariable(Variable v){
		variables.put(v, null);
	}
	
	public void ajouterTerme(Terme terme) {
		termes.add(terme);
	}
	
	
	//variables//peut changer du hashmap à la liste
	public List<Variable> variables() {
		List<Variable> var = new ArrayList<Variable>(variables.keySet());
		Collections.sort(var);
		return var;
	}
	
	public String afficher() {
		String res = "   ";
		for(Terme t : termes) {
			res+= t.toString() + and;
		}
		return res.substring(0, res.length() - and.length());
	}
//	
//	
//	private static List<String> etendreTerme(String terme, List<String> variables) {
//		List<String> termes = new LinkedList<String>();
//		for (int i = 0 ; i < variables.size() ; i++ ) {
//			String var = variables.get(i);
//			if (!terme.contains(var)) {
//				termes = etendreTerme("" + terme + var, variables);
//				termes.addAll(etendreTerme("" + terme + not + var, variables));
//				
//				return termes;
//			}
//		}
//		
//		termes.add(terme);
//		return termes;
//		
//		
//		
	//on suppose que les termes sont triés
	public void etendre() {
		List<Terme> nvTermes = new ArrayList<Terme>();
		for (Terme t : termes) {
			List<Variable> vars = new ArrayList<Variable>();
			vars.addAll(this.variables());
			vars.removeAll(t.variables());
			nvTermes.addAll(t.etendre(vars));
		}
		termes = nvTermes;
	}
	
	public void enleverDoublons() {
		List<Terme> nvTermes = new ArrayList<Terme>();
		for (Terme t : termes) {
			System.out.print("Analyse du terme  "+t);
			t.trierTerme();
			if(!t.estDansListe(nvTermes)) {
				System.out.println(" -> "+t);
				nvTermes.add(t);
			}else {
				System.out.println(" : suppression doublon ");
			}
		}
		termes = nvTermes;
	}
	
}
