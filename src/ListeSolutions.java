import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ListeSolutions{

	List<boolean[]> solutions;
	public void afficher() {
		System.out.println("solutions");
		for(boolean[] sol : solutions) {
			afficherSolution(sol);
		}
	}

	private static void afficherSolution(boolean[] solution) {
		for(int i = 0; i<solution.length;i++) {
			System.out.print(((solution[i])?1:0)+", ");
		}
		System.out.println();
	}
	public void ajouterSolution(boolean[] solution) {
		//afficherSolution(solution);
		if(!contientDeja(solution)){
			solutions.add(solution);
		}else{
			//System.out.println("\t existe deja");
		}
		
	}
	public static boolean solutionsSontEgales(boolean[] solution1, boolean[] solution2){
		for(int i=0; i<solution1.length;i++) {
			if (solution1[i]!=solution2[i])
				return false;
		}
		return true;
	}
	public boolean contientDeja(boolean[] solution) {
		for(boolean[] sol : solutions) {
			if(solutionsSontEgales(sol, solution))
				return true;
		}
		return false;
	}
	public ListeSolutions(int n) {
		solutions = new ArrayList<boolean[]>();
	}
	
	
	private static boolean[]  solutionFixe(List<Variable> variables, Terme t){
		int n = variables.size();
		boolean[] sol = new boolean[n];
		for(int i = 0; i < t.positifs.size() ;i++) {
			Variable v = t.positifs.get(i);
			if ( v == null ) {
				v = t.negatifs.get(i);
				sol[v.id] = false;
			} else {
				sol[v.id] = true;
			}
		}
		return sol;
	}
	
	public ListeSolutions trouverSolutions(Formule f) {
		List<Variable> variables = f.variables();
		for(Terme t:f.termes){
			//System.out.println("terme : "+t);
			trouverSolutions(variables, t);
		}
		return this;
	}
	
	public ListeSolutions trouverSolutions(List<Variable> variables, Terme t) {
		boolean[] fixe = solutionFixe(variables, t);
		List<Variable> vars = new ArrayList<Variable>();
		vars.addAll(variables);
		vars.removeAll(t.variables());
		solutionRec(fixe, vars, false);
		return this;
	}
	
	//ajoute les solutions possibles
	public void solutionRec(boolean[] table, List<Variable> variables, boolean invert){
		if(variables.size() != 0) {
			List<Variable> vars = new ArrayList<Variable>();
			vars.addAll(variables);
			
			int nextPos = vars.remove(0).id;
			if(invert){
				table[nextPos] = true;
				solutionRec(table, vars, false);
				table[nextPos] = false;
				solutionRec(table, vars, true);
			}else{
				table[nextPos] = false;
				solutionRec(table, vars, false);
				table[nextPos] = true;
				solutionRec(table, vars, true);
				table[nextPos] = true;
				table[nextPos] = false;
			}
		}else{
			ajouterSolution(Arrays.copyOf(table, table.length));
		}
		
	}
	
	

}
