import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class TermeTest {
	
	Formule formule;
	
	@Before
	public void init() {
		formule = new Formule(5);
	}
	
	@Test
	public void nbVar(int n) {
		assertEquals(formule.variables().size(),n);
	}

}
